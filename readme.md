<h2 align="center">
    Laravel Mail Editor (Forked MailEclipse)
</h2>

<p align="center">
<a href="https://packagist.org/packages/qoraiche/laravel-mail-editor" alt="sponsors on Open Collective"><img src="https://poser.pugx.org/qoraiche/laravel-mail-editor/v/stable" /></a> <a href="https://packagist.org/packages/qoraiche/laravel-mail-editor" alt="Sponsors on Open Collective"><img src="https://poser.pugx.org/qoraiche/laravel-mail-editor/license" /></a> 
<a href="https://packagist.org/packages/qoraiche/laravel-mail-editor" alt="Sponsors on Open Collective"><img src="https://poser.pugx.org/qoraiche/laravel-mail-editor/downloads" /></a> 
</p>
<br/><br/>

MailEclipse is a mailable editor package for your Laravel applications to create and manage mailables using a web UI. You can use this package to develop mailables without using the command line, and edit templates associated with mailables using a WYSIWYG editor, among other features.

## WORK IN PROGRESS

Please note that this package is still under active development. We encourage everyone to try it and give feedback.

## Features

* Create mailables without using command line.
* Preview/Edit all your mailables at a single place.
* Templates (more than 20+ ready to use email templates).
* WYSIWYG Email HTML/Markdown editor.
* Suitable for laravel beginners.
* and many more... (promise).
